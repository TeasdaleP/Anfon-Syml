const main = require('./main');
const users = require('./users');
const templates = require('./templates');
const communications = require('./communications');
const customers = require('./customers');

module.exports = {
    main,
    users,
    templates,
    customers,
    communications
};
